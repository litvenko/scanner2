﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guievoe_prilosjenie {
	public partial class Main : Form {
		public static Main reference;
		public FileManager files;
		public Main() {
			InitializeComponent();
			files = new FileManager();
			reference = this;
			Options.Load();
		}

		private void Main_Load( object sender, EventArgs e ) {

		}

		private void buttonScan_Click( object sender, EventArgs e ) {
			Scanner.Scan();
			files.Save();
		}

		private void buttonFilters_Click( object sender, EventArgs e ) {
			Filterer.Filter();
			files.Save();
		}

		private void buttonModel_Click( object sender, EventArgs e ) {
			Modeller.CreateModel();
			files.Save();
		}

		private void buttonVisualize_Click( object sender, EventArgs e ) {
			Visualizer.Visualize();
		}

		private void buttonOK_Click( object sender, EventArgs e ) {
			files.Upload( this.textModelName.Text );
		}

		private void numericRed_ValueChanged( object sender, EventArgs e ) {
			Options.red = (short)numericRed.Value;
		}

		private void numericGreen_ValueChanged( object sender, EventArgs e ) {
			Options.green = (short)numericGreen.Value;
		}

		private void numericBlue_ValueChanged( object sender, EventArgs e ) {
			Options.blue = (short)numericBlue.Value;
		}

		private void numericRadius_ValueChanged( object sender, EventArgs e ) {
			Options.filterRadius = (short)numericRadius.Value;
		}

		private void numericWidth_ValueChanged( object sender, EventArgs e ) {
			Options.width = (short)numericWidth.Value;
		}

		private void numericHeight_ValueChanged( object sender, EventArgs e ) {
			Options.height = (short)numericHeight.Value;
		}

		private void numericAngleStep_ValueChanged( object sender, EventArgs e ) {
			Options.scanStep = (short)numericAngleStep.Value;
		}

        private void logLabel_Click(object sender, EventArgs e) {

        }

        private void textModelName_TextChanged( object sender, EventArgs e ) {
            files.Upload( this.textModelName.Text );
        }
    }
}
