﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace guievoe_prilosjenie {
	class Modeller {
		public static void CreateModel() {

			Logger.WriteText( "Creating model " + Main.reference.files.modelName );
			if (Main.reference.files.matrixData == null) {
				Logger.WriteText( "Error: Filters not found." );				
				return;
			}
			Matrix front = null;
			foreach (Matrix filter in Main.reference.files.matrixData) {
				if (filter.angle == 0) {
					front = filter;
					break;
				}
			}
			if (front == null) {
				Logger.WriteText( "Error: Angle 0 not found." );
				return;
			}
			Main.reference.files.model = new Matrix3( front.w, front.w, front.h );
			foreach (Matrix filter in Main.reference.files.matrixData) {
				Main.reference.files.model.ApplyFrame( filter );
				Logger.WriteText( "Angle " + filter.angle + " applied to the model." );
			}
			Logger.WriteText("Model created.");
		}
	}
}
