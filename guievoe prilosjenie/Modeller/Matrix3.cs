﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/* Written by Vlad & KoStick
 * Version 1.3
 * Stable build
 * Copyright 2016 Hornpub.
 */

namespace guievoe_prilosjenie {
	public class Matrix3 {
		public int depth; // x
		public int width;  // y
		public int height; // z
		public bool[,,] data;

		// File I/O

		public Matrix3( string filePath ) {
			FileStream stream = new FileStream( filePath, FileMode.Open, FileAccess.Read );
			BinaryReader reader = new BinaryReader( stream );
			this.depth = reader.ReadInt32();
			this.width = reader.ReadInt32();
			this.height = reader.ReadInt32();
            this.data = new bool[depth, width, height];
			for (int d = 0; d < this.depth; d++) {
				for (int w = 0; w < this.width; w++) {
					for (int h = 0; h < this.height; h++) {
						data[d, w, h] = reader.ReadBoolean();
					}
				}
			}
            reader.Dispose();
            stream.Dispose();
		}

		public void Write( string filePath ) {
			FileStream stream = new FileStream( filePath, FileMode.Create, FileAccess.Write );

			BinaryWriter writer = new BinaryWriter( stream );
			
			writer.Write( this.depth );
			writer.Write( this.width );
			writer.Write( this.height );
			for (int d = 0; d < this.depth; d++) {
				for (int w = 0; w < this.width; w++) {
					for (int h = 0; h < this.height; h++) {
						writer.Write( data[d, w, h] );
					}
					writer.Flush();
				}
			}
            writer.Dispose();
            stream.Dispose();
		}

		// Calculator part

		public Matrix3( int depth, int width, int height ) {
			this.depth = depth;
			this.width = width;
			this.height = height;
			this.data = new bool[depth, width, height];
			for (int x = 0; x < depth; x++) {
				for (int y = 0; y < width; y++) {
					for (int z = 0; z < height; z++) {
						data[x, y, z] = true;
					}
				}
			}
		}

		public void ApplyFrame( Matrix m ) {
			if ((m.w != this.width) || (m.h != this.height)) {
				Console.WriteLine( "Matrix3 tried to cut with wrong sized matrix! (" + m.w + "/" + this.width + ";" + m.h + "/" + this.height + ")" );
				Console.ReadKey( true );
				return;
			}
			int offsetX = this.depth / 2;
			int offsetY = this.width / 2;
            Parallel.For( 0, this.width, y => {
                for ( int x = 0; x < this.depth; x++ ) {
                    int _y = (int)Math.Round(
                        (y - offsetY) * Math.Cos( m.angle / 180.0 * Math.PI )
                      + (x - offsetX) * Math.Sin( m.angle / 180.0 * Math.PI )
                        ) + offsetY;

                    for ( int z = 0; z < this.height; z++ ) {
                        if ( _y >= 0 && _y < this.width ) {
                            if ( !m.data[z, _y] ) {
                                this.data[x, y, z] = false;
                            }
                        }
                    }
                }
            } );
		}
	}
}
