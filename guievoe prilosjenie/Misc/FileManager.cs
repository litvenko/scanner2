﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace guievoe_prilosjenie {
    public class FileManager {
        public const string PHOTO_DIR = "photos";
        public const string MATRIX_DIR = "filters";
        public const string MODEL_NAME = "model";
        public string modelName;
        public List<Perspective> photoData;
        public List<Matrix> matrixData;
        public Matrix3 model;

        public void ClearPhotos() {
            if (photoData != null) {
                foreach (Perspective photo in photoData) {
                    photo.data.Dispose();
                }
                photoData = null;
            }
        }

        public void Upload(string modelName) {
            this.modelName = modelName;
            Main.reference.textModelName.Text = modelName;
            string place = Directory.GetCurrentDirectory();
            ClearPhotos();
            string dir = Path.Combine(place, modelName, PHOTO_DIR);
            if (Directory.Exists(dir)) {
                string[] buffer = Directory.GetFiles(dir);
                foreach (string s in buffer) {
                    if (Path.GetExtension(s).Equals(".png")) {
                        if (photoData == null) {
                            photoData = new List<Perspective>();
                        }
                        photoData.Add(new Perspective(new Bitmap(s), int.Parse(Path.GetFileNameWithoutExtension(s))));
                    }
                }
            }
            this.matrixData = null;
            dir = Path.Combine(place, modelName, MATRIX_DIR);
            if (Directory.Exists(dir)) {
                string[] buffer = Directory.GetFiles(dir);
                foreach (string s in buffer) {
                    if (Path.GetExtension(s).Equals(".dat")) {
                        if (matrixData == null) {
                            matrixData = new List<Matrix>();
                        }
                        matrixData.Add(new Matrix(s));
                    }
                }
            }
            this.model = null;
            dir = Path.Combine(place, modelName);
            if (Directory.Exists(dir)&&File.Exists(Path.Combine(dir, MODEL_NAME + ".dt3"))) {
                model = new Matrix3(Path.Combine(dir, MODEL_NAME + ".dt3"));
            }
            CheckData();
        }

        private void CheckData() {
            Main.reference.infoPhotos.Visible = (photoData != null);
            Main.reference.infoFilters.Visible = (matrixData != null);
            Main.reference.infoModel.Visible = (model != null);
        }

        public void Save() {
            string place = Directory.GetCurrentDirectory();
            if (photoData != null) {
                string dir = Path.Combine(place, modelName, PHOTO_DIR);
                if (!Directory.Exists(dir)) {
                    Directory.CreateDirectory(dir);
                }
                foreach (Perspective photo in photoData) {
                    try {
                        photo.data.Save(Path.Combine(dir, photo.angle + ".png"));
                    }
                    catch (System.Runtime.InteropServices.ExternalException e) {
                        //Logger.WriteText(e.Message);
                    }
                }
            }
            if (matrixData != null) {
                string dir = Path.Combine(place, modelName, MATRIX_DIR);
                if (!Directory.Exists(dir)) {
                    Directory.CreateDirectory(dir);
                }
                foreach (Matrix matrix in matrixData) {
                    matrix.Write(Path.Combine(dir, matrix.angle + ".dat"));
                }
            }
            if (model != null) {
                model.Write(Path.Combine(place, modelName, MODEL_NAME + ".dt3"));
            }
            Upload(this.modelName);
        }
        public void Dispose() {
            model = null;
            matrixData = null;
            photoData = null;
        }
    }
}
