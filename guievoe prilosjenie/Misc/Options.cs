﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace guievoe_prilosjenie{
	abstract class Options {
		public static int width;
		public static int height;
		public static short filterRadius;
		public static int red;
		public static int green;
		public static int blue;
		public static int scanStep;
		public static void Load() {
			width = int.Parse( ConfigurationManager.AppSettings["scanWidth"] );
			height = int.Parse( ConfigurationManager.AppSettings["scanHeight"] );
			filterRadius = short.Parse( ConfigurationManager.AppSettings["filterRadius"] );
			red = int.Parse( ConfigurationManager.AppSettings["filterRed"] );
			green = int.Parse( ConfigurationManager.AppSettings["filterGreen"] );
			blue = int.Parse( ConfigurationManager.AppSettings["filterBlue"] );
			scanStep = int.Parse( ConfigurationManager.AppSettings["angleStep"] );
            Main.reference.files.Upload(ConfigurationManager.AppSettings["defaultModel"]);
			Main.reference.numericRed.Value = red;
			Main.reference.numericGreen.Value = green;
			Main.reference.numericBlue.Value = blue;
			Main.reference.numericRadius.Value = filterRadius;
			Main.reference.numericWidth.Value = width;
			Main.reference.numericHeight.Value = height;
			Main.reference.numericAngleStep.Value = scanStep;
		}
	}
}
