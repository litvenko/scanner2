﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace guievoe_prilosjenie {
	abstract class Logger {
		private static StreamWriter logger = null;
		public static void WriteText( string text ) {
			if (logger == null) {
				logger = new StreamWriter( new FileStream( Path.Combine( Directory.GetCurrentDirectory(), "log.txt" ), FileMode.Create, FileAccess.Write ) );
			}
			try {
				logger.WriteLine( DateTime.Now.ToString() + " " + text );
				logger.Flush();
			} catch (Exception e) {}
			Main.reference.logLabel.Text = text;
		}
		public static void Close() {
			if (logger != null) {
				logger.Dispose();
			}
		}
	}
}
