﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/* Written by Vlad & KoStick
 * Version 1.4
 * Stable build
 * Copyright 2016 Hornpub.
 */

namespace guievoe_prilosjenie {
	public class Matrix {
		public bool[,] data;
		public int h;
		public int w;
		public int angle; // Used for Matrix3 ApplyFilter()

		public Matrix( int h, int w ) {
			this.h = h;
			this.w = w;
			data = new bool[h, w];
		}

		// File I/O

		public Matrix( string filePath ) {
			FileStream stream = new FileStream( filePath, FileMode.Open, FileAccess.Read );
			if (!Path.GetExtension(filePath).Equals( ".txt" )) {
				BinaryReader reader = new BinaryReader( stream );
				this.h = reader.ReadInt32();
				this.w = reader.ReadInt32();
                angle = int.Parse(Path.GetFileNameWithoutExtension(filePath));
                this.data = new bool[this.h, this.w];
				for (int h = 0; h < this.h; h++) {
					for (int w = 0; w < this.w; w++) {
						data[h, w] = reader.ReadBoolean();
					}
				}
                reader.Dispose();
			} else {
				StreamReader reader = new StreamReader( stream );
				List<bool[]> tempBinData = new List<bool[]>();

				while (true) {
					string tempStr = reader.ReadLine();
					if (tempStr == null)
						break;
					bool[] tempBoolArr = new bool[tempStr.Length];
					for (int i = 0; i < tempStr.Length; i++) {
						tempBoolArr[i] = tempStr[i] == '1';
					}
					tempBinData.Add( tempBoolArr );
				}

				h = tempBinData.Count;
				w = tempBinData[0].Length;
				angle = int.Parse( Path.GetFileNameWithoutExtension(filePath) );

				data = new bool[h, w];
				for (int i = 0; i < h; i++) {
					for (int j = 0; j < w; j++) {
						data[i, j] = tempBinData[i][j];
					}
                }
                reader.Dispose();
            }
            stream.Dispose();
        }
		
		public void Write( String filePath ) {
			FileStream stream = new FileStream( filePath, FileMode.Create, FileAccess.Write );
			if (!Path.GetExtension(filePath).Equals( ".txt" )) {
                BinaryWriter writer = new BinaryWriter( stream );
				writer.Write( this.h );
				writer.Write( this.w );
				for (int h = 0; h < this.h; h++) {
					for (int w = 0; w < this.w; w++) {
						writer.Write( data[h, w] );
                    }
                    writer.Flush();
                }
                writer.Dispose();
            } else {
				StreamWriter writer = new StreamWriter( stream );
				for (int h = 0; h < this.h; h++) {
					for (int w = 0; w < this.w; w++) {
						writer.Write( data[h, w] ? 1 : 0 );
                    }
                    writer.Flush();
                    writer.WriteLine();
				}
				writer.Dispose();
            }
            stream.Dispose();
        }

		// Calculator part

		public class MatrixCompararor : IComparer<Matrix> { // Used for sorting matrix in one folder by angle
			public int Compare( Matrix x, Matrix y ) {
				return x.angle == y.angle ? 0 : x.angle < y.angle ? -1 : 1;
			}
		}
	}
}