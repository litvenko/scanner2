﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using AForge.Imaging.Filters;

namespace guievoe_prilosjenie {
	abstract class Filterer {

		private static Bitmap FilterImage( Bitmap source, Color color ) {
			EuclideanColorFiltering filter2 = new EuclideanColorFiltering();
			ResizeBilinear filter1 = new ResizeBilinear( Options.width, Options.height );
			Bitmap newImage = filter1.Apply( source );
			filter2.CenterColor = new AForge.Imaging.RGB( color );
			filter2.FillColor = new AForge.Imaging.RGB( color );
			filter2.FillOutside = false;
			filter2.Radius = Options.filterRadius;
			filter2.ApplyInPlace( newImage );
			return newImage;
		}

		public static void Filter() {
			Logger.WriteText( "Running Filter for model " + Main.reference.files.modelName );
			if (Main.reference.files.photoData != null) {
				Main.reference.files.matrixData = new List<Matrix>();
				Color filterColor = Color.FromArgb( Options.red, Options.green, Options.blue );
				foreach (Perspective photo in Main.reference.files.photoData) {
					Logger.WriteText( "Creating filter for angle " + photo.angle );
					Bitmap filtered = FilterImage( photo.data, filterColor );
					Matrix newFilter = new Matrix( filtered.Height, filtered.Width );
					newFilter.angle = photo.angle;
					for (int y = 0; y < filtered.Height; y++) {
						for (int x = 0; x < filtered.Width; x++) {
							Color color = filtered.GetPixel( x, y );
							newFilter.data[y, x] = !(color.R == filterColor.R && color.G == filterColor.G && color.B == filterColor.B);
						}
					}
					Logger.WriteText( "Filter for angle " + newFilter.angle + " created." );
					Main.reference.files.matrixData.Add( newFilter );
				}
				Logger.WriteText( "Filters created successfully." );
			} else {
				Logger.WriteText( "Error: No photos found for " + Main.reference.files.modelName );
			}
		}
	}
}
