﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace guievoe_prilosjenie {
	public class Perspective {
		public Bitmap data;
		public int angle;
		public Perspective( Bitmap data, int angle ) {
			this.data = data;
			this.angle = angle;
		}
	}
}
