using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;
using System.Configuration;

namespace guievoe_prilosjenie {
	abstract class Scanner {
		private static Bitmap[] photos;
		private static int scanDex;

		private static void video_NewFrame( object sender, NewFrameEventArgs eventA ) {
			photos[scanDex] = (Bitmap)eventA.Frame.Clone();
		}

        public static void Scan() {
            Logger.WriteText( "Running Scan for model " + Main.reference.files.modelName + " " );
            var C = new Communicator();
            try { C.Initialize(); } catch {
                Logger.WriteText( "Failed to connect with Arduino platform" );
                return;
            }
            Logger.WriteText( "Arduino platform found" );
            VideoCaptureDevice videoSource;
            try {
                videoSource = new VideoCaptureDevice(new FilterInfoCollection(FilterCategory.VideoInputDevice)[0].MonikerString);
            } catch (Exception e) {
                Logger.WriteText( "Failed to initialize video input" );
                return;
            }
            videoSource.NewFrame += video_NewFrame;
            scanDex = 0;
            photos = new Bitmap[180 / Options.scanStep + 1];
            Main.reference.files.photoData = new List<Perspective>();
            Thread.Sleep( 5000 );
            videoSource.Start();
            for (int i = 0; i <= 180; i += Options.scanStep) {
                Logger.WriteText( "Scanning angle " + i + " degrees" );
                if ( i > 0.0 ) {
                    C.SendCommand( string.Format( @"G0 R{0:F0} S{1:F0}", (double)i, 12.0 ) );
                    var answer = C.WaitForAnswer();
                    if ( answer != @"OK" ) {
						Logger.WriteText("Arduino platform is incorrect.");
						C = null;
						videoSource.SignalToStop();
						videoSource = null;
                        return;
                    }
                }
				Main.reference.files.photoData.Add(new Perspective(photos[scanDex],i));
				scanDex++;
                Logger.WriteText( "Angle " + i + " saved to internal memory" );
            }
			//C.Close();
			C = null;
			videoSource.SignalToStop();
			videoSource = null;
			Logger.WriteText( "Scan completed successfully." );
		}
	}
}
