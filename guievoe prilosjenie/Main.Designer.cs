﻿namespace guievoe_prilosjenie {
	partial class Main {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing ) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            this.buttonScan = new System.Windows.Forms.Button();
            this.buttonFilters = new System.Windows.Forms.Button();
            this.buttonModel = new System.Windows.Forms.Button();
            this.buttonVisualize = new System.Windows.Forms.Button();
            this.textModelName = new System.Windows.Forms.TextBox();
            this.infoPhotos = new System.Windows.Forms.Label();
            this.infoFilters = new System.Windows.Forms.Label();
            this.infoModel = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.logLabel = new System.Windows.Forms.Label();
            this.numericRadius = new System.Windows.Forms.NumericUpDown();
            this.numericBlue = new System.Windows.Forms.NumericUpDown();
            this.labelBlue = new System.Windows.Forms.Label();
            this.numericGreen = new System.Windows.Forms.NumericUpDown();
            this.labelGreen = new System.Windows.Forms.Label();
            this.numericRed = new System.Windows.Forms.NumericUpDown();
            this.labelRadius = new System.Windows.Forms.Label();
            this.labelRed = new System.Windows.Forms.Label();
            this.labelBackground = new System.Windows.Forms.Label();
            this.panelOptions = new System.Windows.Forms.Panel();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.labelAngleStep = new System.Windows.Forms.Label();
            this.numericAngleStep = new System.Windows.Forms.NumericUpDown();
            this.labelWidth = new System.Windows.Forms.Label();
            this.numericWidth = new System.Windows.Forms.NumericUpDown();
            this.labelScanSettings = new System.Windows.Forms.Label();
            this.labelHeight = new System.Windows.Forms.Label();
            this.numericHeight = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRed)).BeginInit();
            this.panelOptions.SuspendLayout();
            this.panelSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAngleStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHeight)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonScan
            // 
            this.buttonScan.Location = new System.Drawing.Point(12, 12);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(69, 40);
            this.buttonScan.TabIndex = 0;
            this.buttonScan.Text = "Scan";
            this.buttonScan.UseVisualStyleBackColor = true;
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            // 
            // buttonFilters
            // 
            this.buttonFilters.Location = new System.Drawing.Point(12, 58);
            this.buttonFilters.Name = "buttonFilters";
            this.buttonFilters.Size = new System.Drawing.Size(69, 40);
            this.buttonFilters.TabIndex = 1;
            this.buttonFilters.Text = "Create filters";
            this.buttonFilters.UseVisualStyleBackColor = true;
            this.buttonFilters.Click += new System.EventHandler(this.buttonFilters_Click);
            // 
            // buttonModel
            // 
            this.buttonModel.Location = new System.Drawing.Point(12, 104);
            this.buttonModel.Name = "buttonModel";
            this.buttonModel.Size = new System.Drawing.Size(69, 40);
            this.buttonModel.TabIndex = 2;
            this.buttonModel.Text = "Create model";
            this.buttonModel.UseVisualStyleBackColor = true;
            this.buttonModel.Click += new System.EventHandler(this.buttonModel_Click);
            // 
            // buttonVisualize
            // 
            this.buttonVisualize.Location = new System.Drawing.Point(12, 150);
            this.buttonVisualize.Name = "buttonVisualize";
            this.buttonVisualize.Size = new System.Drawing.Size(69, 40);
            this.buttonVisualize.TabIndex = 3;
            this.buttonVisualize.Text = "Visualize";
            this.buttonVisualize.UseVisualStyleBackColor = true;
            this.buttonVisualize.Click += new System.EventHandler(this.buttonVisualize_Click);
            // 
            // textModelName
            // 
            this.textModelName.Location = new System.Drawing.Point(87, 11);
            this.textModelName.Name = "textModelName";
            this.textModelName.Size = new System.Drawing.Size(245, 20);
            this.textModelName.TabIndex = 4;
            this.textModelName.Text = "Your model here";
            // 
            // infoPhotos
            // 
            this.infoPhotos.AutoSize = true;
            this.infoPhotos.Location = new System.Drawing.Point(87, 144);
            this.infoPhotos.Name = "infoPhotos";
            this.infoPhotos.Size = new System.Drawing.Size(118, 13);
            this.infoPhotos.TabIndex = 0;
            this.infoPhotos.Text = "Scanned photos found!";
            this.infoPhotos.Visible = false;
            // 
            // infoFilters
            // 
            this.infoFilters.AutoSize = true;
            this.infoFilters.Location = new System.Drawing.Point(87, 157);
            this.infoFilters.Name = "infoFilters";
            this.infoFilters.Size = new System.Drawing.Size(67, 13);
            this.infoFilters.TabIndex = 1;
            this.infoFilters.Text = "Filters found!";
            this.infoFilters.Visible = false;
            // 
            // infoModel
            // 
            this.infoModel.AutoSize = true;
            this.infoModel.Location = new System.Drawing.Point(87, 170);
            this.infoModel.Name = "infoModel";
            this.infoModel.Size = new System.Drawing.Size(69, 13);
            this.infoModel.TabIndex = 2;
            this.infoModel.Text = "Model found!";
            this.infoModel.Visible = false;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(298, 11);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(34, 20);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // logLabel
            // 
            this.logLabel.Location = new System.Drawing.Point(87, 170);
            this.logLabel.Name = "logLabel";
            this.logLabel.Size = new System.Drawing.Size(245, 23);
            this.logLabel.TabIndex = 7;
            this.logLabel.Text = "Welcome to Scanner 0.4a!";
            this.logLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.logLabel.Click += new System.EventHandler(this.logLabel_Click);
            // 
            // numericRadius
            // 
            this.numericRadius.Location = new System.Drawing.Point(60, 96);
            this.numericRadius.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericRadius.Name = "numericRadius";
            this.numericRadius.Size = new System.Drawing.Size(44, 20);
            this.numericRadius.TabIndex = 3;
            this.numericRadius.ValueChanged += new System.EventHandler(this.numericRadius_ValueChanged);
            // 
            // numericBlue
            // 
            this.numericBlue.Location = new System.Drawing.Point(60, 70);
            this.numericBlue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericBlue.Name = "numericBlue";
            this.numericBlue.Size = new System.Drawing.Size(44, 20);
            this.numericBlue.TabIndex = 2;
            this.numericBlue.ValueChanged += new System.EventHandler(this.numericBlue_ValueChanged);
            // 
            // labelBlue
            // 
            this.labelBlue.AutoSize = true;
            this.labelBlue.Location = new System.Drawing.Point(26, 72);
            this.labelBlue.Name = "labelBlue";
            this.labelBlue.Size = new System.Drawing.Size(28, 13);
            this.labelBlue.TabIndex = 5;
            this.labelBlue.Text = "Blue";
            // 
            // numericGreen
            // 
            this.numericGreen.Location = new System.Drawing.Point(60, 44);
            this.numericGreen.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericGreen.Name = "numericGreen";
            this.numericGreen.Size = new System.Drawing.Size(44, 20);
            this.numericGreen.TabIndex = 1;
            this.numericGreen.ValueChanged += new System.EventHandler(this.numericGreen_ValueChanged);
            // 
            // labelGreen
            // 
            this.labelGreen.AutoSize = true;
            this.labelGreen.Location = new System.Drawing.Point(18, 46);
            this.labelGreen.Name = "labelGreen";
            this.labelGreen.Size = new System.Drawing.Size(36, 13);
            this.labelGreen.TabIndex = 6;
            this.labelGreen.Text = "Green";
            // 
            // numericRed
            // 
            this.numericRed.Location = new System.Drawing.Point(60, 18);
            this.numericRed.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericRed.Name = "numericRed";
            this.numericRed.Size = new System.Drawing.Size(44, 20);
            this.numericRed.TabIndex = 0;
            this.numericRed.ValueChanged += new System.EventHandler(this.numericRed_ValueChanged);
            // 
            // labelRadius
            // 
            this.labelRadius.AutoSize = true;
            this.labelRadius.Location = new System.Drawing.Point(18, 98);
            this.labelRadius.Name = "labelRadius";
            this.labelRadius.Size = new System.Drawing.Size(40, 13);
            this.labelRadius.TabIndex = 7;
            this.labelRadius.Text = "Radius";
            // 
            // labelRed
            // 
            this.labelRed.AutoSize = true;
            this.labelRed.Location = new System.Drawing.Point(27, 20);
            this.labelRed.Name = "labelRed";
            this.labelRed.Size = new System.Drawing.Size(27, 13);
            this.labelRed.TabIndex = 4;
            this.labelRed.Text = "Red";
            // 
            // labelBackground
            // 
            this.labelBackground.AutoSize = true;
            this.labelBackground.Location = new System.Drawing.Point(3, 2);
            this.labelBackground.Name = "labelBackground";
            this.labelBackground.Size = new System.Drawing.Size(104, 13);
            this.labelBackground.TabIndex = 8;
            this.labelBackground.Text = "Background settings";
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.labelBackground);
            this.panelOptions.Controls.Add(this.labelRed);
            this.panelOptions.Controls.Add(this.labelRadius);
            this.panelOptions.Controls.Add(this.numericRed);
            this.panelOptions.Controls.Add(this.labelGreen);
            this.panelOptions.Controls.Add(this.numericGreen);
            this.panelOptions.Controls.Add(this.labelBlue);
            this.panelOptions.Controls.Add(this.numericBlue);
            this.panelOptions.Controls.Add(this.numericRadius);
            this.panelOptions.Location = new System.Drawing.Point(224, 37);
            this.panelOptions.Name = "panelOptions";
            this.panelOptions.Size = new System.Drawing.Size(108, 128);
            this.panelOptions.TabIndex = 8;
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.labelAngleStep);
            this.panelSettings.Controls.Add(this.numericAngleStep);
            this.panelSettings.Controls.Add(this.labelWidth);
            this.panelSettings.Controls.Add(this.numericWidth);
            this.panelSettings.Controls.Add(this.labelScanSettings);
            this.panelSettings.Controls.Add(this.labelHeight);
            this.panelSettings.Controls.Add(this.numericHeight);
            this.panelSettings.Location = new System.Drawing.Point(87, 37);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(131, 104);
            this.panelSettings.TabIndex = 9;
            // 
            // labelAngleStep
            // 
            this.labelAngleStep.AutoSize = true;
            this.labelAngleStep.Location = new System.Drawing.Point(21, 76);
            this.labelAngleStep.Name = "labelAngleStep";
            this.labelAngleStep.Size = new System.Drawing.Size(57, 13);
            this.labelAngleStep.TabIndex = 14;
            this.labelAngleStep.Text = "Angle step";
            // 
            // numericAngleStep
            // 
            this.numericAngleStep.Location = new System.Drawing.Point(84, 74);
            this.numericAngleStep.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericAngleStep.Name = "numericAngleStep";
            this.numericAngleStep.Size = new System.Drawing.Size(44, 20);
            this.numericAngleStep.TabIndex = 13;
            this.numericAngleStep.ValueChanged += new System.EventHandler(this.numericAngleStep_ValueChanged);
            // 
            // labelWidth
            // 
            this.labelWidth.AutoSize = true;
            this.labelWidth.Location = new System.Drawing.Point(43, 24);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(35, 13);
            this.labelWidth.TabIndex = 11;
            this.labelWidth.Text = "Width";
            // 
            // numericWidth
            // 
            this.numericWidth.Location = new System.Drawing.Point(84, 22);
            this.numericWidth.Maximum = new decimal(new int[] {
            1920,
            0,
            0,
            0});
            this.numericWidth.Name = "numericWidth";
            this.numericWidth.Size = new System.Drawing.Size(44, 20);
            this.numericWidth.TabIndex = 9;
            this.numericWidth.ValueChanged += new System.EventHandler(this.numericWidth_ValueChanged);
            // 
            // labelScanSettings
            // 
            this.labelScanSettings.Location = new System.Drawing.Point(3, 3);
            this.labelScanSettings.Name = "labelScanSettings";
            this.labelScanSettings.Size = new System.Drawing.Size(125, 16);
            this.labelScanSettings.TabIndex = 9;
            this.labelScanSettings.Text = "Scan settings";
            this.labelScanSettings.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelHeight
            // 
            this.labelHeight.AutoSize = true;
            this.labelHeight.Location = new System.Drawing.Point(40, 50);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(38, 13);
            this.labelHeight.TabIndex = 12;
            this.labelHeight.Text = "Height";
            // 
            // numericHeight
            // 
            this.numericHeight.Location = new System.Drawing.Point(84, 48);
            this.numericHeight.Maximum = new decimal(new int[] {
            1080,
            0,
            0,
            0});
            this.numericHeight.Name = "numericHeight";
            this.numericHeight.Size = new System.Drawing.Size(44, 20);
            this.numericHeight.TabIndex = 10;
            this.numericHeight.ValueChanged += new System.EventHandler(this.numericHeight_ValueChanged);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 202);
            this.Controls.Add(this.panelSettings);
            this.Controls.Add(this.infoPhotos);
            this.Controls.Add(this.infoModel);
            this.Controls.Add(this.panelOptions);
            this.Controls.Add(this.infoFilters);
            this.Controls.Add(this.logLabel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonVisualize);
            this.Controls.Add(this.buttonModel);
            this.Controls.Add(this.buttonFilters);
            this.Controls.Add(this.buttonScan);
            this.Controls.Add(this.textModelName);
            this.Name = "Main";
            this.Text = "Main menu";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRed)).EndInit();
            this.panelOptions.ResumeLayout(false);
            this.panelOptions.PerformLayout();
            this.panelSettings.ResumeLayout(false);
            this.panelSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAngleStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonScan;
		private System.Windows.Forms.Button buttonFilters;
		private System.Windows.Forms.Button buttonModel;
		private System.Windows.Forms.Button buttonVisualize;
		public System.Windows.Forms.Label infoPhotos;
		public System.Windows.Forms.Label infoFilters;
		public System.Windows.Forms.Label infoModel;
		private System.Windows.Forms.Button buttonOK;
		public System.Windows.Forms.Label logLabel;
		public System.Windows.Forms.NumericUpDown numericRadius;
		public System.Windows.Forms.NumericUpDown numericBlue;
		private System.Windows.Forms.Label labelBlue;
		public System.Windows.Forms.NumericUpDown numericGreen;
		private System.Windows.Forms.Label labelGreen;
		public System.Windows.Forms.NumericUpDown numericRed;
		private System.Windows.Forms.Label labelRadius;
		private System.Windows.Forms.Label labelRed;
		private System.Windows.Forms.Label labelBackground;
		private System.Windows.Forms.Panel panelOptions;
		private System.Windows.Forms.Panel panelSettings;
		private System.Windows.Forms.Label labelWidth;
		public System.Windows.Forms.NumericUpDown numericWidth;
		private System.Windows.Forms.Label labelScanSettings;
		private System.Windows.Forms.Label labelHeight;
		public System.Windows.Forms.NumericUpDown numericHeight;
		private System.Windows.Forms.Label labelAngleStep;
		public System.Windows.Forms.NumericUpDown numericAngleStep;
        public System.Windows.Forms.TextBox textModelName;
    }
}

