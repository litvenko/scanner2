﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace guievoe_prilosjenie {
	abstract class Visualizer {
		public static void Visualize() {
			if (Main.reference.files.model == null) {
				Logger.WriteText("Error: model not found.");
				return;
			}
			using (var window = new MainWindow()) {
				window.Run( 30 );
			}
		}
	}
}
