using System;
using System.Drawing;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Collections.Generic;

namespace guievoe_prilosjenie {
	internal sealed class MainWindow : GameWindow {
		private Matrix3 _matrix;
        private List<Tuple<int, int, int, int>> _voxels;
		private int _countX = 0;
		private float _rotationX = 0.0f;
		private float _rotationZ = 0.0f;
		private float _offsetX = 0.0f;
		private float _offsetY = 0.0f;
		private float _offsetZ = 0.0f;
		private float _scale = 1.0f;

        public MainWindow() {
			LoadMatrix();
		}

		protected override void OnLoad( EventArgs e ) {
			base.OnLoad( e );

			VSync = VSyncMode.On;

            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Normalize);
            GL.Enable(EnableCap.Lighting);

            // Enable light 0
            float[] light0Position = { -1.0f, 0.0f, 1.0f, 0.0f };
            float[] light0Ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0Diffuse = { 0.7f, 0.3f, 0.3f, 1.0f };

            GL.Light(LightName.Light0, LightParameter.Position, light0Position);
            GL.Light(LightName.Light0, LightParameter.Ambient, light0Ambient);
            GL.Light(LightName.Light0, LightParameter.Diffuse, light0Diffuse);

            GL.Enable(EnableCap.Light0);

            // Enable light 1
            float[] light1Position = { 1.0f, 0.0f, 1.0f, 0.0f };
            float[] light1Ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light1Diffuse = { 0.3f, 0.7f, 0.3f, 1.0f };

            GL.Light(LightName.Light1, LightParameter.Position, light1Position);
            GL.Light(LightName.Light1, LightParameter.Ambient, light1Ambient);
            GL.Light(LightName.Light1, LightParameter.Diffuse, light1Diffuse);

            GL.Enable(EnableCap.Light1);

            // Enable light 2
            float[] light2Position = { 0.0f, 1.0f, 0.0f, 0.0f };
            float[] light2Ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light2Diffuse = { 0.3f, 0.3f, 0.7f, 1.0f };

            GL.Light(LightName.Light2, LightParameter.Position, light2Position);
            GL.Light(LightName.Light2, LightParameter.Ambient, light2Ambient);
            GL.Light(LightName.Light2, LightParameter.Diffuse, light2Diffuse);

            GL.Enable(EnableCap.Light2);
        }

		protected override void OnResize( EventArgs e ) {
			base.OnResize( e );

			GL.Viewport( 0, 0, Width, Height );

			// Set projection transformation
			var aspect = (float)Width / Height;
			var perspective = Matrix4.CreatePerspectiveFieldOfView( 1.0f, aspect, 0.1f, 5.0f );

			GL.MatrixMode( MatrixMode.Projection );

			GL.LoadIdentity();

			GL.MultMatrix( ref perspective );
		}

		protected override void OnKeyDown( KeyboardKeyEventArgs e ) {
			if (e.Key == Key.Escape && !e.IsRepeat) {
				Exit();
			}

			/*if (e.Key == Key.PageUp && !e.IsRepeat) {
				_fileIndex -= 1;

				LoadMatrix();
			}

			if (e.Key == Key.PageDown && !e.IsRepeat) {
				_fileIndex += 1;

				LoadMatrix();
			}*/
		}

		protected override void OnUpdateFrame( FrameEventArgs e ) {

			base.OnUpdateFrame( e );

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.Up ) ) {
				_rotationX -= 1.0f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.Down) ) {
				_rotationX += 1.0f; 
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.Left ) ) {
				_rotationZ -= 1.0f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.Right ) ) {
				_rotationZ += 1.0f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.A ) ) {
				_offsetX -= .01f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.D ) ) {
				_offsetX += .01f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.W ) ) {
				_offsetY += .01f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.S ) ) {
				_offsetY -= .01f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.Q ) ) {
				_offsetZ -= .01f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.E ) ) {
				_offsetZ += .01f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.KeypadPlus ) ) {
				_scale *= 1.01f;
			}

			if ( OpenTK.Input.Keyboard.GetState().IsKeyDown( Key.KeypadMinus ) ) {
				_scale *= 0.99f;
			}

			Title = string.Format( @"{0} | {1} | pitch = {2} yaw = {3} scale = {4}", _matrix.width, _matrix.height, _rotationX, _rotationZ, _scale );
		}

		protected override void OnRenderFrame( FrameEventArgs e ) {
			base.OnRenderFrame( e );

			GL.Clear( ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit );

			var lookat = Matrix4.LookAt(
				_offsetX*_scale,
				_offsetY*_scale-3.0f,
				_offsetZ*_scale,
				_offsetX*_scale,
				_offsetY*_scale,
				_offsetZ*_scale,
				0.0f, 0.0f, 1.0f);

			GL.MatrixMode( MatrixMode.Modelview );

			GL.LoadIdentity();

			GL.MultMatrix( ref lookat );

			GL.Rotate( _rotationX, 1.0f, 0.0f, 0.0f );
			GL.Rotate( _rotationZ, 0.0f, 0.0f, 1.0f );
			GL.Scale( _scale, _scale, _scale );

			DrawVoxels();

			SwapBuffers();
		}

		private void DrawVoxels() {
			if ( _voxels == null ) {
				return;
			}

			var factor = 1.0f / _countX;

			foreach ( var voxel in _voxels ) {
				DrawVoxel( voxel.Item4,
				  (voxel.Item1 - _matrix.depth / 2) * factor,
				  (_matrix.width / 2 - voxel.Item2) * factor,
				  (_matrix.height / 2 - voxel.Item3) * factor, factor, Color.White );
			}
		}

		private void DrawVoxel( int type, float x, float y, float z, float size, Color color ) {
			size *= 0.5f;

			GL.Begin( PrimitiveType.Quads );

            if (type >= 32) {
                type -= 32;
                // Front
                GL.Color3( color );

                GL.Normal3( 1.0f, 0.0f, 0.0f );

                GL.Vertex3( x + size, y + size, z + size );
                GL.Vertex3( x + size, y - size, z + size );
                GL.Vertex3( x + size, y - size, z - size );
                GL.Vertex3( x + size, y + size, z - size );
            }

            if ( type >= 16 ) {
                type -= 16;
                // Left
                GL.Color3( color );

                GL.Normal3( 0.0f, -1.0f, 0.0f );

                GL.Vertex3( x + size, y - size, z + size );
                GL.Vertex3( x - size, y - size, z + size );
                GL.Vertex3( x - size, y - size, z - size );
                GL.Vertex3( x + size, y - size, z - size );
            }
            
            if ( type >= 8 ) {
                type -= 8;
                // Top
                GL.Color3( color );

                GL.Normal3( 0.0f, 0.0f, -1.0f );

                GL.Vertex3( x + size, y + size, z - size );
                GL.Vertex3( x + size, y - size, z - size );
                GL.Vertex3( x - size, y - size, z - size );
                GL.Vertex3( x - size, y + size, z - size );
            }
            if ( type >= 4 ) {
                type -= 4;
                // Back
                GL.Color3( color );

                GL.Normal3( -1.0f, 0.0f, 0.0f );

                GL.Vertex3( x - size, y + size, z + size );
                GL.Vertex3( x - size, y + size, z - size );
                GL.Vertex3( x - size, y - size, z - size );
                GL.Vertex3( x - size, y - size, z + size );
            }

            if ( type >= 2 ) {
                type -= 2;
                // Top
                GL.Color3( color );

                GL.Normal3( 0.0f, 1.0f, 0.0f );

                GL.Vertex3( x + size, y + size, z + size );
                GL.Vertex3( x + size, y + size, z - size );
                GL.Vertex3( x - size, y + size, z - size );
                GL.Vertex3( x - size, y + size, z + size );
            }

            if ( type >= 1 ) {
                type -= 1;
                // Bot
                GL.Color3( color );

                GL.Normal3( 0.0f, 0.0f, 1.0f );

                GL.Vertex3( x + size, y + size, z + size );
                GL.Vertex3( x - size, y + size, z + size );
                GL.Vertex3( x - size, y - size, z + size );
                GL.Vertex3( x + size, y - size, z + size );
            }
            GL.End();
		}

		private void LoadMatrix() {
			_matrix = Main.reference.files.model;
			_voxels = new List<Tuple<int, int, int,int>>();
			_countX = _matrix.depth;

			for ( int d = 0; d < _matrix.depth; d++ ) {
				for ( int w = 0; w < _matrix.width; w++ ) {
                    for ( int h = 0; h < _matrix.height; h++ ) {
                        if(!_matrix.data[d,w,h]) { continue; }
                        int i = 63;
                        if ( h != 0 && _matrix.data[d, w, h - 1] ) {
                            i -= 1; // Bot
                        }
                        if ( w != 0 && _matrix.data[d, w - 1, h] ) {
                            i -= 2; // right
                        }
                        if ( d != 0 && _matrix.data[d - 1, w, h] ) {
                            i -= 4; // Back
                        }
                        if ( h != _matrix.height - 1 && _matrix.data[d, w, h+1] ) {
                            i -= 8; // Top
                        }
                        if ( w != _matrix.width - 1 && _matrix.data[d, w+1, h] ) {
                            i -= 16; // left
                        }
                        if ( d != _matrix.depth - 1 && _matrix.data[d+1, w, h] ) {
                            i -= 32; // front
                        }
                        if ( i == 0 ) {
                            continue; }
						_voxels.Add( new Tuple<int, int, int,int>( d,w,h,i ) );
					}
				}
			}
		}
	}
}
